package com.surojd.sim.registration.validation;

import com.surojd.sim.registration.dao.SimRepository;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidatorId implements ConstraintValidator<ValidateId, String> {

  @Autowired
  private SimRepository userRepository;
  private ValidateId id;

  @Override
  public void initialize(ValidateId id) {
    this.id = id;
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return userRepository.findById(value).isPresent();
  }
}
