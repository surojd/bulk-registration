package com.surojd.sim.registration.dto;

import com.surojd.sim.registration.enums.Gender;
import com.surojd.sim.registration.enums.SimType;
import com.surojd.sim.registration.validation.ValidateId;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class SimDto {

  @NotNull
  @Pattern(regexp = "^(\\+[1-9][0-9]*(\\([0-9]*\\)|-[0-9]*-))?[0]?[1-9][0-9\\- ]*$", message = "MSISDN should comply to country's standard (e.g. +66)")
  private String MSISDN;
  @NotNull
  private SimType SIM_TYPE;
  @NotNull
  @Pattern(regexp = "^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)", message = "Enter Full Name")
  private String NAME;
  @NotNull
  @Past(message = "DATE_OF_BIRTH shouldn't be TODAY or FUTURE")
  private Date DATE_OF_BIRTH;
  @NotNull
  private Gender GENDER;
  @NotNull
  @Size(min = 20, message = "Address must at least be 20 characters long")
  private String ADDRESS;
  @NotNull
  @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "ID_NUMBER should be a mix of characters & numbers")
  private String ID_NUMBER;
}
