package com.surojd.sim.registration.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UploadSimCsv {

  private MultipartFile file;
}
