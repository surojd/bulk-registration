package com.surojd.sim.registration.enums;

public enum SimType {
  PREPAID, POSTPAID
}
