package com.surojd.sim.registration.database;

import com.surojd.sim.registration.enums.Gender;
import com.surojd.sim.registration.enums.SimType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
@Table
public class Sim implements Serializable {

  @Id
  @Pattern(regexp = "^(\\+[1-9][0-9]*(\\([0-9]*\\)|-[0-9]*-))?[0]?[1-9][0-9\\- ]*$", message = "MSISDN should comply to country's standard (e.g. +66)")
  private String MSISDN;
  @NotNull
  @Enumerated(EnumType.STRING)
  private SimType SIM_TYPE;
  @NotNull
  @Pattern(regexp = "^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)", message = "Enter Full Name")
  private String NAME;
  @NotNull
  @Temporal(TemporalType.DATE)
  @Past(message = "DATE_OF_BIRTH shouldn't be TODAY or FUTURE")
  private Date DATE_OF_BIRTH;
  @NotNull
  @Enumerated(EnumType.STRING)
  private Gender GENDER;
  @NotNull
  @Size(min = 20, message = "Address must at least be 20 characters long")
  private String ADDRESS;
  @NotNull
  private String ID_NUMBER;

}
