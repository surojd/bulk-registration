package com.surojd.sim.registration.controller;

import com.surojd.sim.registration.dto.SimDto;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimRestController {

  @PostMapping("api/validate")
  public String validate(@RequestBody @Valid SimDto dto, BindingResult result) {
    if (result.hasErrors()) {
      ObjectError get = result.getAllErrors().get(0);
      return get.getDefaultMessage();
    }
    return "";
  }

}
