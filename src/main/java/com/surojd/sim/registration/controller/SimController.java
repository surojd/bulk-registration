package com.surojd.sim.registration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.surojd.sim.registration.dao.SimRepository;
import com.surojd.sim.registration.database.Sim;
import com.surojd.sim.registration.dto.SimDtoRequest;
import com.surojd.sim.registration.dto.UploadSimCsv;
import com.surojd.sim.registration.enums.Gender;
import com.surojd.sim.registration.enums.SimType;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@Controller
public class SimController {

  SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
  private final ModelMapper modelMapper = new ModelMapper();
  ObjectMapper mapper = new ObjectMapper();
  @Autowired
  SimRepository simRepository;
  RestTemplate restTemplate = new RestTemplate();

  @RequestMapping(value = "/sim", method = RequestMethod.POST)
  public String page(Model model, @ModelAttribute UploadSimCsv csv, BindingResult result) {
    try {
      InputStream file = csv.getFile().getInputStream();
      Scanner scan = new Scanner(file);
      boolean first = true;
      while (scan.hasNextLine()) {
        if (first) {
          first = false;
          continue;
        }
        String nextLine = scan.nextLine();
        String[] split = nextLine.split(",");
        if (split.length > 6) {
          Date date = null;
          try {
            date = format.parse(split[3]);
          } catch (ParseException ex) {
          }
          String address = "";
          for (int i = 4; i < split.length - 1; i++) {
            address += split[i];
          }
          try {
            SimDtoRequest dto = SimDtoRequest.builder()
              .MSISDN(split[0])
              .SIM_TYPE(SimType.valueOf(split[1]))
              .NAME(split[2])
              .DATE_OF_BIRTH(date)
              .GENDER(Gender.valueOf(split[4]))
              .ADDRESS(address)
              .ID_NUMBER(split[split.length - 1])
              .build();

            HttpEntity<SimDtoRequest> request = new HttpEntity<>(dto);
            ResponseEntity<String> postForEntity = restTemplate.postForEntity("http://localhost:8080/api/validate", request, String.class);
            if (!postForEntity.getBody().isEmpty()) {
              throw new Exception(postForEntity.getBody());
            }
            Sim sim = modelMapper.map(dto, Sim.class);
            if (simRepository.findById(sim.getMSISDN()).isPresent()) {
              throw new Exception("MSISDN can't be duplicated");
            }

            Sim save = simRepository.save(sim);
            Logger.getLogger(SimController.class.getName()).log(Level.INFO, save.getMSISDN() + " send Sms.");
            File f = new File(save.getMSISDN().replace("+", "") + ".txt");
            try (FileWriter fileWriter = new FileWriter(f)) {
              fileWriter.append(mapper.writeValueAsString(save));
            }
          } catch (Exception e) {
            Logger.getLogger(SimController.class.getName()).log(Level.SEVERE, e.getMessage());
          }
        }
      }
    } catch (Exception ex) {
      Logger.getLogger(SimController.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "redirect:/success.html";
  }

}
