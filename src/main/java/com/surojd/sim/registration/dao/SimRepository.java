package com.surojd.sim.registration.dao;

import com.surojd.sim.registration.database.Sim;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SimRepository extends JpaRepository<Sim, String> {

}
